#include <stdio.h>
#include "list.h"
#include "kernel.h"

static char * error_color = "";
static void ok(bool assertion, char *msg)
{
    if (!assertion) {
	printf("\t%sError: '%s' is unsatisfied.\n", error_color, msg);
	exit(-1);
    } else {
	printf("\tSuccess: %s\n", msg);
    }
}

void create_list_test()
{
    puts("create_list_test");
    list *result = create_list();
    ok(result != NULL, "list is not null");
    ok(result->pHead != NULL, "list has one valid head");
    ok(result->pTail != NULL, "list has one valid tail");
}

void insert_ready_test()
{
    puts("insert_ready_test");
    list *ready_list = create_list();
    listobj *objs[5];
    int size = sizeof(objs)/sizeof(listobj*);
    for (int i=0; i<5; ++i) {
	listobj *obj = malloc(sizeof(listobj));
	TCB *task = malloc(sizeof(TCB));
	task->DeadLine = size - i;
	obj->pTask = task;
	objs[i] = obj;
	insert_ready(ready_list, obj);
    }
    char str[50];
    listobj *iterator = ready_list->pHead->pNext;
    for(int i=0; i<5; ++i) {
	sprintf(str, "%dth element is in the right position of ready list",
		i+1);
	ok(objs[size-i-1] == iterator, str);
	iterator = iterator->pNext;
    }
}

void insert_timer_test()
{
    puts("insert_timer_test");
    list *timer_list = create_list();
    listobj *objs[5];
    int size = sizeof(objs)/sizeof(listobj*);
    for(int i=0; i<5; ++i) {
	listobj *obj = malloc(sizeof(listobj));
	obj->nTCnt = size - i;
	objs[i] = obj;
	insert_timer(timer_list, obj);
    }
    char str[50];
    listobj *iterator = timer_list->pHead->pNext;
    for(int i=0; i<5; ++i) {
	sprintf(str, "%dth element is in the right position of timer list",
		i+1);
	ok(objs[size-i-1] == iterator, str);
	iterator = iterator->pNext;
    }
}

void extract_first_test()
{
    puts("extract_first_test");
    list *_list = create_list();
    listobj *objs[5];
    int size = sizeof(objs)/sizeof(listobj*);
    for(int i=0; i<size; ++i) {
	listobj *obj = malloc(sizeof(listobj));
	obj->nTCnt = i;
	objs[i] = obj;
	insert_timer(_list, obj);
    }
    ok(objs[0] == _list->pHead->pNext, "before: obj[0] in first position");
    extract_first(_list);
    ok(objs[1] == _list->pHead->pNext, "after: obj[1] in first position");
}

void extract_obj_test()
{
    puts("extract_obj_test");
    list *_list = create_list();
    listobj *objs[5];
    int size = sizeof(objs)/sizeof(listobj*);
    for(int i=0; i<size; ++i) {
	listobj *obj = malloc(sizeof(listobj));
	obj->nTCnt = i;
	objs[i] = obj;
	insert_timer(_list, obj);
    }
    ok(objs[1]->pNext == objs[2], "second obj'next is third obj");
    ok(objs[3]->pPrevious == objs[2], "forth obj's previous is third obj");
    extract_obj(objs[2]);
    ok(objs[1]->pNext == objs[3], "second obj'next is forth obj");
    ok(objs[3]->pPrevious == objs[1], "forth obj's previous is second obj");


}

int main() {
    create_list_test();
    insert_ready_test();
    insert_timer_test();
    extract_first_test();
    extract_obj_test();
    return 0;
}
