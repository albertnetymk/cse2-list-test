#ifndef _LIST_H
#include "kernel.h"
list *create_list();
void insert_ready(list *ready_list, listobj *obj);
void insert_timer(list *timer_list, listobj *obj);
void extract_first(list *generic_list);
void extract_obj(listobj *obj);
#endif
